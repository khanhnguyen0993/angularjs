angular
.module("ngProducts", ['ngMaterial', 'ui.router', 'firebase'])
.config(function($mdThemingProvider, $stateProvider){
  $mdThemingProvider.theme('default')
  .primaryPalette('green')
  .accentPalette('orange');

    // use the HTML5 History API
    // $locationProvider.html5Mode(true);

    $stateProvider
    .state('products', {
      url: '/applestore', 
      templateUrl: 'components/products/products.tpl.html',
      controller: 'productsCtrl as vm'
    })
    .state('products.new', {
      url: '/new', 
      templateUrl: 'components/products/new/products.new.tpl.html',
      controller: 'newProductsCtrl as vm'
    })
    .state('products.edit', {
      url: '/edit/:id', 
      templateUrl: 'components/products/edit/products.edit.tpl.html',
      controller: 'editProductsCtrl as vm',
      params: {
        classified: null
      }
    })
  });

