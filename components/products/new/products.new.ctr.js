(()=>{
  "use strict";

  angular
  .module('ngProducts')
  .controller('newProductsCtrl', function($scope, $state, $mdSidenav, $mdDialog, productsFactory, $timeout){
      var vm = this; // capture variable
      vm.closeSidebar = closeSidebar;
      vm.saveProduct = saveProduct;

      $timeout(()=>{
        $mdSidenav('left').open();
        $scope.$watch('vm.sidenavOpen', sidenav => {
          if (sidenav === false) {
            $mdSidenav('left')
            .close()
            .then(()=>{
              $state.go('products');
            });
          }
        });
      });

      function closeSidebar(){
        vm.sidenavOpen = false;
      }


      function saveProduct(product){
        console.log(product);
        if(product){
          product.contact = {
            name: "Khanh Nguyen",
            phone: "(999) 999-9999",
            email: "khanh@gmail.com"
          }

          $scope.$emit('newProduct', product);
          vm.sidenavOpen = false;
        }
      }

    });
})();