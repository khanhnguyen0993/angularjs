(() => {

  "use strict";

  angular
    .module('ngProducts')
    .factory('productsFactory', function($http, $firebaseArray) {
      var ref = new Firebase('https://ngclassifieds-c5f8e.firebaseio.com/');
      return {
        ref: $firebaseArray(new Firebase('https://ngclassifieds-c5f8e.firebaseio.com/'))
      }
    });
})();
