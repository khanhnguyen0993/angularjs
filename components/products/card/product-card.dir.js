(()=>{
  "use strict";

  angular
  .module("ngProducts")
  .directive("productCard", function(){
    return {
      templateUrl: "components/products/card/product-card.tpl.html",
      scope: {
        products: "=products",
        productsFilter: "=productsFilter",
        category: "=category"
      },
      controller: productCardController,
      controllerAs: "vm",
    }

    function productCardController($state, $scope, $mdDialog, productsFactory, $mdToast){
      var vm = this;
      vm.products = productsFactory.ref;
      vm.editProduct = editProduct;
      vm.deleteProduct = deleteProduct;

      function editProduct (product){
        $state.go('products.edit', {
          id: product.$id
        });
      }

      function deleteProduct (product, event){
        var confirm = $mdDialog.confirm()
        .title(`Are you sure you want to delete ${product.title}?`)
        .ok('Yes')
        .cancel('No')
        .targetEvent(event);
        $mdDialog.show(confirm).then(()=>{
          vm.products.$remove(product);
          showToast('Product deleted !');
        }, ()=>{        });
      }

      function showToast(message){
        $mdToast.show(
          $mdToast.simple()
          .content(message)
          .position("top, right")
          .hideDelay(3000)
        );
      }


    }
  });
})();