(()=>{

  "use strict";

  angular
  .module("ngProducts")
  .controller("productsCtrl", function($scope, $state, $http, productsFactory, $mdSidenav, $mdToast, $mdDialog) {
    var vm = this;
    
      vm.openSidebar = openSidebar;
      vm.closeSidebar = closeSidebar;
      vm.saveProduct = saveProduct;
      vm.editProduct = editProduct;
      vm.saveEdit = saveEdit;
      vm.deleteProduct = deleteProduct; 

      vm.products;
      vm.categories;
      vm.editing;
      vm.product;

      vm.products = productsFactory.ref;
      vm.products.$loaded().then(products => {
        vm.categories = getCategories(products);
      });

      $scope.$on('newProduct', (event, product)=>{
        vm.products.$add(product);
        showToast('Product Saved !');
      });

      $scope.$on('editSaved', (event, message)=>{
        showToast(message);
      });

      var contact = {
        name: "Khanh Nguyen",
        phone: "(999) 999-9999",
        email: "khanh@gmail.com"
      }

      function openSidebar(){ $state.go('products.new'); }
      function closeSidebar (){ $mdSidenav('left').close(); }
      
      // this product is the obj sent from fronend
      function saveProduct(product){
        product.contact = contact;
        if (product) vm.products.push(product);
        vm.product = {};
        closeSidebar();
        showToast("Product Save!");
      }

      function editProduct (product){
        $state.go('products.edit', {
          id: product.$id
        });
      }

      function saveEdit (){
        vm.editing = false;
        vm.product = {};
        closeSidebar();
        showToast("Edit Save!");
      }

      function deleteProduct (product, event){
        var confirm = $mdDialog.confirm()
        .title(`Are you sure you want to delete ${product.title}?`)
        .ok('Yes')
        .cancel('No')
        .targetEvent(event);
        $mdDialog.show(confirm).then(()=>{
          vm.products.$remove(product);
          showToast('Product deleted !');
        }, ()=>{        });
      }

      function showToast(message){
        $mdToast.show(
          $mdToast.simple()
          .content(message)
          .position("top, right")
          .hideDelay(3000)
          );
      }

      function getCategories(products){
        var categories = [];
        angular.forEach(products, item=>{
          angular.forEach(item.categories, category=>{
            categories.push(category);
          });
        });
        return _.uniq(categories);
      }
      
    });
})();