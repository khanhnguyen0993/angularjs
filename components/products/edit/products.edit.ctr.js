(()=>{
  "use strict";

  angular
  .module('ngProducts')
  .controller('editProductsCtrl', function($scope, $state, $mdSidenav, $mdDialog, productsFactory, $timeout){
      var vm = this; // capture variable
      vm.products = productsFactory.ref;
      vm.closeSidebar = closeSidebar;
      vm.saveEdit = saveEdit;
      vm.product = vm.products.$getRecord($state.params.id);

      $timeout(()=>{
        $mdSidenav('left').open();
        $scope.$watch('vm.sidenavOpen', sidenav => {
          if (sidenav === false) {
            $mdSidenav('left')
            .close()
            .then(()=>{
              $state.go('products');
            });
          }
        });
      });

      function closeSidebar(){
        vm.sidenavOpen = false;
      }

      function saveEdit(){
        vm.products.$save(vm.product).then(()=>{
          $scope.$emit('editSaved', 'Edit Saved !');
          vm.sidenavOpen = false;
        });
      }

    });
})();